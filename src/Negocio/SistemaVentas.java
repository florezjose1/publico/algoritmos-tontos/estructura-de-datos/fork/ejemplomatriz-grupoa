/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Vendedor;

/**
 * Clase del sistema de ventas
 * @author madarme
 */
public class SistemaVentas {
    
    private Vendedor equipoVentas[];

    public SistemaVentas() {
     
    }

    public SistemaVentas(int numVendedores) {
        
        this.equipoVentas=new Vendedor[numVendedores];
     
    }
    
    
    
    public Vendedor[] getEquipoVentas() {
        return equipoVentas;
    }

    public void setEquipoVentas(Vendedor[] equipoVentas) {
        this.equipoVentas = equipoVentas;
    }
    
    
    
    
}
